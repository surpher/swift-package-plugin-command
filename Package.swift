// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "DownloaderPlugin",
    products: [
        .plugin(name: "DownloaderPlugin", targets: ["DownloaderPlugin"]),
    ],
    dependencies: [
        .package(url: "https://github.com/apple/swift-argument-parser", from: "1.3.1"),
        .package(url: "https://github.com/apple/swift-crypto.git", from: "3.0.0"),
    ],
    targets: [
        .executableTarget(
            name: "CryptoWrapper",
            dependencies: [
                .product(name: "Crypto", package: "swift-crypto"),
                .product(name: "ArgumentParser", package: "swift-argument-parser"),
            ],
            path: "Crypto"
        ),

        .plugin(
            name: "DownloaderPlugin",
            capability: .command(
              intent: .custom(
                verb: "download-ffi",
                description: "Downloads libpact_ffi binary from github.com/pact-foundation/pact-reference/release"
              )
            ),
            dependencies: [
                "CryptoWrapper"
            ]
        ),
    ]
)
